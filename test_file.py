row_count = 10000
col_count = 100
cat_val = [0, 1]
col_range = list(range(col_count))


def divide(lst, n):
    p = len(lst) // n
    if len(lst) - p > 0:
        return [lst[:p]] + divide(lst[p:], n - 1)
    else:
        return [lst]


col_list = divide(col_range, 3)
string_col = col_list[0]
categorical_col = col_list[1]
numeric_col = col_list[2]
string_col = divide(string_col, 3)
gender_col = string_col[0]
ip_col = string_col[1]
date_col = string_col[2]
print("Date col", date_col)
print("Ip col", ip_col)
